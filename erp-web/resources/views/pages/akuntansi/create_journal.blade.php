@extends('theme.default')

@section('breadcrumb')
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">List Akun</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item active" aria-current="page">Akuntansi</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
@endsection

@section('content')
<div class="container-fluid">
    <!-- basic table -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">List Akun</h4>
                    <form method="POST" action="{{ URL::to('akuntansi/storejournal') }}" >
                      @csrf
                      <div class="form-row">
                          <div class="col-6 mb-3">
                                <label>Tanggal</label>  
                                <input type="date" value="{{date('Y-m-d')}}" class='form-control' required placeholder='Tanggal' name="tanggal">
                          </div>
                           @if($user->role_id == 1)
                          <div class="col-6 mb-3">
                                <label>Lokasi</label>  
                                <select class="form-control select2" name="location_id" id="location_id" style="width: 100%;" required>
                                  <option>Pilih Lokasi</option>
                                  @foreach($business_locations as $value)
                                    <option value="{{$value->id}}">{{$value->name}}</option>
                                  @endforeach
                                </select>
                          </div>
                          @endif
                      </div>
                      <div class="form-row">
                          <div class="col-12">
                            <label>Deskripsi : *</label>
                          </div>
                          <div class="col-md-12 mb-3">
                            <textarea id="deskripsi" name="deskripsi" class="form-control" required placeholder='Keterangan Jurnal'></textarea>
                          </div>
                      </div>
                    
                      <div class="form-row">
                          <div class="col-4">
                                <label>Akun</label>    
                                <select class="form-control select2" name="akun" id="akun" style="width: 100%;" required>
                                    @foreach($data['akun_option'] as $key => $value)
                                    <option value="{{$key}}">{{$value}}</option>
                                    @endforeach
                                </select>
                          </div>
                          <div class="col-4">
                            <label>Jumlah</label>    
                              <input type="number" class="form-control" name="jumlah_akun" id="jumlah_akun" required onkeyup="cekJumlahLawan()">
                          </div>
                          <div class="col-4">
                                <label>Tipe</label>    
                                <select class="form-control select2" name="tipe_akun" id="tipe_akun" style="width: 100%;" required>
                                    <option value="">Pilih Tipe</option>
                                    <option value="1">Debit</option>
                                    <option value="0">Kredit</option>
                                </select>
                          </div>
                      </div>
                      <br>
                      <hr>
                      <div class="form-row">
                          <div class="col-4">
                                <label>Lawan Akun</label>    
                                <select class="form-control select2" name="lawan_akun[]" id="lawan_akun[]" style="width: 100%;" required>
                                    @foreach($data['akun_option'] as $key => $value)
                                    <option value="{{$key}}">{{$value}}</option>
                                    @endforeach
                                </select>
                          </div>
                          <div class="col-4">
                            <label>Jumlah</label>    
                              <input type="number" class="form-control" name="jumlah_lawan_akun[]" id="jumlah_lawan_akun[]" required onkeyup="cekJumlahLawan()">
                          </div>
                          <div class="col-4">
                                <label>Tipe</label>    
                                <select class="form-control select2" name="tipe_lawan_akun[]" id="tipe_lawan_akun[]" style="width: 100%;" required>
                                    <option value="">Pilih Tipe</option>
                                    <option value="1">Debit</option>
                                    <option value="0">Kredit</option>
                                </select>
                          </div>
                      </div>
                      <br>
                      <div id="input_lawan_akun">
                      </div>
                      <div class="col-12">
                          <div align="right">
                              <br>
                              <button id="add_lawan_akun"><i class="fa fa-plus"></i>Tambah</button>
                          </div>
                      </div>
                      <button class="btn btn-primary mt-4" id="submit" type="submit">Submit</button>                
                  </form>
                </div>
            </div>
        </div>
    </div>
                
</div>


<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
        var max_input=10;
        var add_lawan_akun=$('#add_lawan_akun');
        var input_lawan_akun=$('#input_lawan_akun');
        var y = 1; //initlal text box count
        $(add_lawan_akun).click(function(e){
            e.preventDefault();
            var akun_option='<option value="">Pilih Akun / No Akun</option>';
            var akun_lawan=@php echo $data['akun_option_js'] @endphp;
            for (var i = 0; i < akun_lawan.length; i++) {
                akun_option+='<option value="'+akun_lawan[i].label+'">'+akun_lawan[i].value+'</option>';
            }

            var input_akun = '<select class="custom-select" id="inputGroupSelect01">'+akun_option+'</select>';
            var input_jumlah = '<input class="form-control" name="jumlah_lawan[]" id="jumlah_lawan[]" required type="number" onkeyup="cekJumlahLawan()">';
            var input_tipe = '<select class="form-control" name="tipe_akun_lawan[]" style="width: 100%;"><option value="1">Debit</option><option value="0">Kredit</option></select>';
            if (y < max_input) {
                y++;
                $(input_lawan_akun).append('<div class="form-row"><div class="col-4"><div class="input-group mb-3"><div class="input-group-prepend"><button class="input-group-text remove_field_obat" id="remove_field_obat" for="inputGroupSelect01">X</button></div>'+input_akun+'</div></div><div class="col-4">'+input_jumlah+'</div><div class="col-4">'+input_tipe+'</div></div>'); //add input box
            }
            
        });
        $(input_lawan_akun).on("click","#remove_field_obat", function(e){ //user click on remove text
            e.preventDefault(); 
            $(this).closest('.form-row').remove(); y--;
            cekJumlahLawan(null);
        });
    });
    function cekJumlahLawan(selectObject = null, isCheckJml = false) {
        var jumlah_length = $("[id^=jumlah_lawan]").length;
        var jumlah_akun = $("[id^=jumlah_akun]").val();
        if (jumlah_akun == '') {
            for (var x = 0; x < jumlah_length; x++) {
                $("[id^=jumlah_lawan]").eq(x).val('');
            }
            alert('jumlah akun belum terisi');
            $('#jumlah_akun').focus();
        }
        // console.log(jumlah_length);
        var total=0;
        for (var x = 0; x < jumlah_length; x++) {
            var lawan = $("[id^=jumlah_lawan]").eq(x).val() != '' ? parseInt($("[id^=jumlah_lawan]").eq(x).val()) : 0;
            total+=lawan;
        }
        if (total == parseInt(jumlah_akun)) {
            $('#submit').attr('disabled', false);
        }else{
            $('#submit').attr('disabled', true);
        }
    }
    function cekLawan(){
        var lawan_length = $("[id^=lawan_akun]").length;
        var jumlah=$("[id^=jumlah_akun]").val()
        for (var x = 0; x < lawan_length; x++) {
            if ($("[id^=lawan_akun]").eq(x).val() == 15) {
                $('#add_lawan_akun').attr('disabled', true);
                $("[id^=jumlah_lawan]").eq(x).val(jumlah);
                $('#submit').attr('disabled', false);
            }else{
                $('#add_lawan_akun').attr('disabled', false);
            }
        };
    }
  
</script>
@endsection
