@extends('theme.default')

@section('breadcrumb')
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">List Akun</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item active" aria-current="page">Akuntansi</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
@endsection

@section('content')
@php
    function formatRupiah($num){
        return number_format($num, 0, '.', '.');
    }
    function formatDate($date){
        $date=date_create($date);
        return date_format($date, 'd-m-Y');
    }
@endphp
<div class="container-fluid">
    <!-- basic table -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <h4 id="titleJurnal">Jurnal Umum Bulan November 2019</h4>
                            <form method="POST" action="{{ URL::to('akuntansi/jurnal') }}" class="form-inline float-right">
                              @csrf
                                <input type="date" name="date" class="form-control" required>&nbsp;
                                <button class="btn btn-success">cari</button>&nbsp;
                                <a class="btn btn-primary pull-right" 
                                href="{{ URL::to('akuntansi/createjournal') }}" >
                                <i class="fa fa-plus"></i> Tambah</a>
                             </form>
                        </div>
                    </div>
                     <br>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th width="200px">Tanggal</th>
                                    <th>Keterangan</th>
                                    <th>Reff</th>
                                    <th>Debit</th>
                                    <th>Kredit</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            @foreach ($data as $key => $value)
                                @php $k=0 @endphp
                                @foreach ($data[$key]['detail'] as $k => $v)
                                    @if($k==0)
                                <tr style="background-color:#3c8dbc; color:white">
                                    <td>{{formatDate($v->tanggal)}}</td>
                                        @if($v->keterangan == 'akun')                                
                                    <td>{{$v->nama_akun}}</td>
                                        @else
                                    <td align="center">{{$v->nama_akun}}</td>
                                        @endif
                                    <td>{{$v->no_akun}}</td>

                                        @if($v->tipe == 'KREDIT')
                                    
                                    <td><div class="text-left"></div></td>
                                    <td><div class="text-right">Rp. {{formatRupiah($v->jumlah)}}</div></td>
                                    <th></th>
                                </tr>
                                        @else

                                    <td><div class="text-right">Rp. {{formatRupiah($v->jumlah)}}</div></td>
                                    <td><div class="text-left"></div></td>
                                    <th></th>
                                </tr>
                                        @endif
                                    @else
                                <tr>
                                    <td></td>
                                        @if($v->keterangan == 'akun')                                
                                    <td>{{$v->nama_akun}}</td>
                                        @else
                                    <td align="center">{{$v->nama_akun}}</td>
                                        @endif
                                    <td>{{$v->no_akun}}</td>
                                        @if($v->tipe == 'KREDIT')
                                    
                                    <td><div class="text-left"></div></td>
                                    <td><div class="text-right">Rp. {{formatRupiah($v->jumlah)}}</div></td>
                                    <th></th>
                                </tr>
                                        @else

                                    <td><div class="text-right">Rp. {{formatRupiah($v->jumlah)}}</div></td>
                                    <td><div class="text-left"></div></td>
                                    <th></th>
                                </tr>
                                        @endif
                                    @endif
                                    
                                    @php $k++ @endphp
                                
                                @endforeach
                                <tr style="background-color:#fefefe">
                                    <th></th>
                                    <th>{{$value['deskripsi']}}</th>
                                    <th colspan="4"></th>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
                
</div>
<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<script type="text/javascript">
    var bulan='{{$date}}';
    console.log(bulan);
    $('#titleJurnal').html('Jurnal Umum '+formatBulan(bulan));
    function formatBulan(val){
        var bulan = ['Januari', 'Februari', 'Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
        val=val.split('-');
        var getMonth=val[1];
        return val[2]+' '+bulan[getMonth-1]+' '+val[0];
    }
</script>
@endsection