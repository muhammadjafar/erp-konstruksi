@extends('theme.default')

@section('breadcrumb')
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">Edit Order</h4>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ URL::to('order') }}">Order</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Edit</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('content')

<style>
.delete{
    background-color:#a57575;
    color:white
}
.checkbox label:after,
.radio label:after {
  content: '';
  display: table;
  clear: both;
}

.checkbox .cr,
.radio .cr {
  position: relative;
  display: inline-block;
  border: 1px solid #a9a9a9;
  border-radius: .25em;
  width: 1.3em;
  height: 1.3em;
  float: left;
  margin-right: .5em;
}

.radio .cr {
  border-radius: 50%;
}

.checkbox .cr .cr-icon,
.radio .cr .cr-icon {
  position: absolute;
  font-size: .8em;
  line-height: 0;
  top: 50%;
  left: 15%;
}

.radio .cr .cr-icon {
  margin-left: 0.04em;
}

.checkbox label input[type="checkbox"],
.radio label input[type="radio"] {
  display: none;
}

.checkbox label input[type="checkbox"]+.cr>.cr-icon,
.radio label input[type="radio"]+.cr>.cr-icon {
  opacity: 0;
}

.checkbox label input[type="checkbox"]:checked+.cr>.cr-icon,
.radio label input[type="radio"]:checked+.cr>.cr-icon {
  opacity: 1;
}

.checkbox label input[type="checkbox"]:disabled+.cr,
.radio label input[type="radio"]:disabled+.cr {
  opacity: .5;
}
</style>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Edit Order</h4>
                    <form method="POST" action="{{ URL::to('order/update') }}" class="mt-4" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                        <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Customer</label>
                                    <input type="hidden" name="order_id" value="{{$order['id']}}">
                                    <select id="customer_id" name="customer_id" required class="form-control select2 custom-select" style="width: 100%; height:32px;">
                                        <option value="">--- Pilih Customer ---</option>
                                        @foreach($customer as $value)
                                        <option value="{{$value['id']}}" {{ $order['customer_id'] == $value['id'] ? 'selected' : '' }}>{{$value['coorporate_name']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Deskripsi Order</label>
                                    <textarea class="form-control" name="order_name" required>{{$order['order_name']}}</textarea>
                                </div>
                            </div>
                            <div class="col-sm-6" hidden>
                                <div class="form-group">
                                    <label>Pilih Produk</label>
                                    <select id="select_product" class="form-control select2 custom-select" style="width: 100%; height:32px;" onchange="addProduct(this.value)">
                                        <option value="">--- Pilih Produk ---</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Pilih Produk</label>
                                    <div class="input-group">
                                    
                                    <input type="text" id='product_search' class="form-control" placeholder="Cari Produk" aria-label="" aria-describedby="basic-addon1">
                                    <div class="input-group-append">
                                        <button class="btn btn-success" type="button" data-toggle="modal" data-target="#myModal" title="Tambah Produk"><i class="mdi mdi-plus-circle"></i></button>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="table-responsive">
                            <h4 class="card-title">Detail Order</h4>
                            <table class="table table-bordered" id="detail-order">
                                <thead>
                                    <tr>
                                        <th>Nama Produk</th>
                                        <th>Deskripsi</th>
                                        <th>Edit as new</th>
                                        <th>Foto Sketch</th>
                                        <th width="150px">Total</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>    
                                <tbody>
                                    @foreach($order_d as $value)
                                    <tr>
                                        <td><input type="hidden" name="order_d_id[]" value="{{$value['id']}}"><input type="hidden" name="id_product[]" value="{{$value['product_id']}}"><p id="name">{{$value['name']}}</p><input type="text" style="display:none" id="inputName" class="form-control" value="{{$value['name']}}" name="name[]"></td>
                                        <td><p id="descriptions">{{$value['description']}}</p><textarea name="deskripsi[]" class="form-control" style="display:none" id="inputDesc" onkeyup="changeDesc(this)">{{$value['description']}}</textarea></td>
                                        <td><input type="checkbox" id="editDesc" class="form-control" onclick="cekChange(this)" name="editProd[]" value="{{$value['product_id']}}" disabled></td>
                                        <td><img src="/upload/product/{{$value['image']}}" width="60px" onclick="showImage(this.src)"><input type="file" id="file" style="display:none" name="file[]" accept="image/*" class="form-control"></td>
                                        <td><input type="number" required class="form-control" placeholder="Total" name="total_produk[]" value="{{$value['total']}}"></td>
                                        <td><button type="button" hidden class="btn btn-sm btn-danger removeOption"><i class="mdi mdi-delete"></i></button>
                                        
                                        <div class="checkbox" title="remove detail">
                                        <label>
                                        <input type="checkbox" id="delRow" class="form-control" onclick="deleteRow(this)" name="deleted_order_d[]" value="{{$value['id']}}">
                                        <!-- <input type="checkbox" value="" onclick="deleteRow(this)"> -->
                                        <span class="cr"><i class="cr-icon mdi mdi-close"></i></span>
                                        </label>
                                        </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <button type="submit" id="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        
        </div>
    </div>
</div>
<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Add Product</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <form id="addProduct" action="javascript:;" accept-charset="utf-8" method="post" enctype="multipart/form-data">
            <div class="modal-body">
                @csrf
                <div class="form-group">
                    <label>Nama</label>
                    <input type="text" class="form-control" name="name" required/>
                </div>
                <div class="form-group">
                    <label>Deskripsi</label>
                    <textarea class="form-control" name="description" required></textarea>
                </div>
                <div class="form-group">
                    <label>Satuan</label>
                    <select id="satuan" name="m_unit_id" required class="form-control select2 custom-select" style="width: 100%; height:32px;">
                        <option value="">--- Select Satuan ---</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Image Sketch</label>
                    <input type="file" class="form-control" name="image"  accept="image/*"/>
                </div>
                <div class="form-group">
                    <label>Price</label>
                    <input type="number" class="form-control" name="price"/>
                </div>
            
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                <button class="btn btn-info waves-effect" id="submit">Submit</button>
            </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- <script src="{!! asset('theme/assets/libs/jquery/dist/jquery.min.js') !!}"></script> -->
<script src="{!! asset('theme/assets/libs/sweetalert2/dist/sweetalert2.all.min.js') !!}"></script>
<script>
var url='{{URL::to('/')}}';
var total_produk={{count($order_d)}};
$(document).ready(function(){
    
    formSatuan = $('[id^=satuan]');
    formSatuan.empty();
    formSatuan.append('<option value="">-- Select Satuan --</option>');
    $.ajax({
        type: "GET",
        url: "{{ URL::to('master_satuan/list') }}", //json get site
        dataType : 'json',
        success: function(response){
            arrData = response['data'];
            for(i = 0; i < arrData.length; i++){
                formSatuan.append('<option value="'+arrData[i]['id']+'">'+arrData[i]['name']+'</option>');
            }
        }
    });
    
    $("form#addProduct").on("submit", function( event ) {
        var form = $('#addProduct')[0];
        var data = new FormData(form);
        event.preventDefault();
        // console.log( $('form#addProduct').serialize() );
        $.ajax({
            type: "POST",
            url: "{{ URL::to('master_product/create_json') }}", //json get site
            dataType : 'json',
            data: data,
            processData: false,
            contentType: false,
            success: function(response){
                $('#product_search').focus();
                $('#addProduct').trigger("reset");
                formSatuan.val('').change();
                alert(response['message']);
            }
        });
        $('#myModal').modal('hide');
    });
    // $('#select_product').select2({
    //     ajax : {
    //         url : '/order/suggest_product',
    //         delay: 200,
    //         dataType : 'json',
    //         processResults: function (data) {
    //             return {
    //             results:  $.map(data, function (item) {
    //                 return {
    //                 text: item.name,
    //                 id: item.id
    //                 }
    //             })
    //             };
    //         },
    //         cache: true
    //     }
    //     // placeholder: "Select Product",
    //     // initSelection: function(element, callback) {                   
    //     // }
    // });

      $( "#product_search" ).autocomplete({
        source: function( request, response ) {
          // Fetch data
          $.ajax({
            url:"/order/fetch",
            type: 'post',
            dataType: "json",
            data: {
               _token: '{{csrf_token()}}',
               search: request.term
            },
            success: function( data ) {
               response( data );
            }
          });
        },
        select: function (event, ui) {
           // Set selection
           $('#product_search').val(''); 
           addProductOrder(ui.item.value);
           $('#product_search').focus();
           return false;
        },
        minLength: 1
      });
    //   .data('ui-autocomplete')._renderItem = function(ul, item){
    //     return $("<li class='ui-autocomplete-row'></li>")
    //         .data("item.autocomplete", item)
    //         .append(item.label)
    //         .appendTo(ul);
    //     };
});
function addProduct(value){
    addProductOrder(value);
    $('#select_product').focus();
}
var wrapper_product_detail = $("#detail-order"); 
function addProductOrder(id){
    var data=[];
    var detail_product=$('#detail-order');
    $.ajax({
        url : '/order/get-product/'+id,
        type : 'GET',
        dataType : 'json',
        async : false,
        success : function(response){
            total_produk++;
            var tdAdd='<tr>'+
                            '<td><input type="hidden" name="order_d_id[]" value="0"><input type="hidden" name="id_product[]" value="'+response['id']+'"><p id="name">'+response['name']+'</p><input type="text" style="display:none" id="inputName" class="form-control" value="'+response['name']+'" name="name[]"></td>'+
                            '<td><p id="descriptions">'+response['description']+'</p><textarea name="deskripsi[]" class="form-control" style="display:none" id="inputDesc" onkeyup="changeDesc(this)">'+response['description']+'</textarea></td>'+
                            '<td><input type="checkbox" id="editDesc" class="form-control" onclick="cekChange(this)" name="editProd[]" value="'+response['id']+'"></td>'+
                            '<td><img src="/upload/product/'+response['image']+'" width="60px" onclick="showImage(this.src)"><input type="file" id="file" style="display:none" name="file[]" accept="image/*" class="form-control"></td>'+
                            '<td><input type="number" required class="form-control" placeholder="Total" name="total_produk[]"></td>'+
                            '<td><button type="button" class="btn btn-sm btn-danger removeOption"><i class="mdi mdi-delete"></i></button></td>'+
                        '</tr>';
            $('#detail-order').find('tbody:last').append(tdAdd);
        }
    })
    cekTotalProduk();
    // console.log(total_produk);
}
$("#detail-order").on("click", ".removeOption", function(event) {
    event.preventDefault();
    $(this).closest("tr").remove();
    total_produk--;
    cekTotalProduk();
});
function showImage(src){
    swal({   
        imageUrl: src,
        showConfirmButton: false
    });
}
function cekTotalProduk(){
    if(total_produk < 1){
        $('#submit').attr('disabled', true);
    }else{
        $('#submit').attr('disabled', false);
    }
}
function changeDesc(value){
    // console.log(value);
    $(value).closest('tr').find('#description').html(value.value);
}
function cekChange(el){
    if($(el).is(':checked')){
        $(el).closest('tr').find('#inputDesc').show();
        $(el).closest('tr').find('#inputName').show();
        $(el).closest('tr').find('#file').show();
        $(el).closest('tr').find('#descriptions').hide();
        $(el).closest('tr').find('#name').hide();
    }else{
        $(el).closest('tr').find('#inputDesc').hide();
        $(el).closest('tr').find('#inputName').hide();
        $(el).closest('tr').find('#file').hide();
        $(el).closest('tr').find('#descriptions').show();
        $(el).closest('tr').find('#name').show();
    }
}
function deleteRow(el){
    if($(el).is(':checked')){
        $(el).closest('tr').addClass("delete");
    }else{
        $(el).closest('tr').removeClass("delete");
    }
}
</script>

@endsection