<?php

namespace App\Http\Controllers\Accounting;

use Illuminate\Http\Response;

use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Exception;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Redirect;
use Carbon\Carbon;

use DB;

class AkuntanController extends Controller
{
    private $base_api_url;
    private $site_id = null;
    private $user_name = null;
    private $user_id = null;
    public function __construct()
    {
        //Authenticate page menu
        $this->middleware(function ($request, $next) {
            Controller::isLogin(auth()->user()['role_id']); 
            $this->user_id = auth()->user()['id'];
            $this->site_id = auth()->user()['site_id'];
            $this->user_name = auth()->user()['name'];
            
            return $next($request);
        });

        $this->base_api_url = env('API_URL');
    }

    // *
    //  * Constructor
    //  *
    //  * @param Util $commonUtil
    //  * @return void
     
    // public function __construct(Util $commonUtil)
    // {
    //     $this->commonUtil = $commonUtil;
    // }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $raw=DB::table('tbl_akun')->get();
        $data1=array();
        foreach ($raw as $key => $value) {
            $main=DB::table('tbl_akun')->where('id_akun', $value->id_main_akun)->first();
            $row=array();
            $row['id_akun']= $value->id_akun;
            $row['no_akun']= $value->no_akun;
            $row['nama_akun']= $value->nama_akun;
            $row['level']= $value->level;
            $row['main']= ($main != null ? $main->nama_akun : '');
            $data1[]=$row;
        }
        
        $data=array(
            'list'  => $data1
        );
        return  view('pages.akuntansi.list_akun', $data);
    }
    public function createAkun(){
        // $data['parent_option']=array();
        $data['parent_option']= DB::table('tbl_akun')->where('level', 0)->get();
        $parent_option_js=array();
        foreach (DB::table('tbl_akun')->where('level', 0)->get() as $key => $value) {
            // $data['parent_option'][$value->id_akun]=$value->nama_akun;
            $parent_option_js[]=array(
                'label'     => $value->id_akun,
                'value'     => $value->no_akun,
            );
        }
        $data['parent_option_js']=json_encode($parent_option_js);

        return  view('pages.akuntansi.create_account', $data);
    }
    public function storeAkun(Request $request)
    {
        
        $row=DB::table('tbl_akun')->where('id_akun', $request->input('id_parent'))->first();
        $id_main_akun=0;
        $level=$request->input('level');
        $nama_akun=$request->input('nama_akun');
        $no_akun=$request->input('no_akun');
        $id_parent=$turunan1=$turunan2=$turunan3=0;
        $id_parent=($request->input('id_parent') ? $request->input('id_parent') : 0);
        $turunan1=($request->input('level2') ? $request->input('level2') : 0);
        $turunan2=($request->input('level3') ? $request->input('level3') : 0);

        if ($level == 1) {
            $id_main_akun=$request->input('id_parent');
        }else if ($level == 2) {
            $id_main_akun=$request->input('level2');
        }else if ($level == 3) {
            $id_main_akun=$request->input('level3');
        }
        $data=array(
            'no_akun'         => $no_akun,
            'nama_akun'       => $nama_akun,
            'level'           => $level,
            'id_main_akun'    => $id_main_akun,
            'sifat_debit'     => $row->sifat_debit,
            'sifat_kredit'    => $row->sifat_kredit,
        );
        DB::table('tbl_akun')->insert($data);
        $row=DB::table('tbl_akun')->max('id_akun');
        $data_d=array(
            'id_akun'           => $row,
            'id_parent'         => $id_parent,
            'turunan1'          => $turunan1,
            'turunan2'          => $turunan2,
            'turunan3'          => $turunan3,
        );
        DB::table('tbl_akun_detail')->insert($data_d);
        return redirect('akuntansi');
    }
    public function getNoAkun($id){
        header('Content-Type: application/json');
        $data=DB::table('tbl_akun')
                ->select(DB::raw('MAX(id_main_akun) as id_main_akun'), DB::raw('MAX(no_akun) as no_akun'))
                ->where('id_main_akun', $id)
                ->first();
        $data2=DB::table('tbl_akun')->where('id_akun', $id)->first();
        $akun= array('no_akun'=>$data->no_akun,'id_main_akun'=>$data->id_main_akun, 'no_akun_main'=>$data2->no_akun);
        echo json_encode($akun);
    }
    public function getLevel($id) {
        header('Content-Type: application/json');
        $data=DB::table('tbl_akun')
                ->where('id_main_akun', $id)
                ->get();
        echo json_encode($data);
    }
    public function jurnal(Request $request)
    {

        $user = DB::table('users')->where('id', $this->user_id)->first();

        $date=date('Y-m-d');
        if ($request->input('date')) {
            $date=$request->input('date');
        }
        $list_trx=DB::table('tbl_trx_akuntansi')
                    ->where('tanggal', $date)
                    ->orderBy('tanggal');
        if ($user->role_id != 1) {
            $list_trx->where('location_id', $user->location_id);
        }
        $results=$list_trx->get();

        $data=array();
        foreach ($results as $key => $value) {
            $data[$key]=array(
                'id_trx_akun' => $value->id_trx_akun,
                'deskripsi' => $value->deskripsi,
                'tanggal'   => $value->tanggal
            );
            $data[$key]['detail']=$this->getDetailKas($value->id_trx_akun);
        }
        return view('pages.akuntansi.journal_list')->with(compact('data', 'date'));
    }
    private function getDetailKas($id)
    {
        $data=DB::table('tbl_trx_akuntansi')
                ->join('tbl_trx_akuntansi_detail', 'tbl_trx_akuntansi.id_trx_akun','=','tbl_trx_akuntansi_detail.id_trx_akun')
                ->join('tbl_akun', 'tbl_akun.id_akun','=','tbl_trx_akuntansi_detail.id_akun')
                ->where('tbl_trx_akuntansi.id_trx_akun', $id)
                ->orderBy('tbl_trx_akuntansi_detail.keterangan')
                ->get();
        return $data;
    }
    public function neraca(Request $request)
    {
       
        $user_id = request()->session()->get('user.id');
        $user = DB::table('users')->where('id', $this->user_id)->first();

        $date=0;
        if ($request->input('bulan')) {
            $date=$request->input('tahun').'-'.$request->input('bulan');
        }else{
            $date=date('Y-m');
        }
        $bulan=json_encode(explode('-', $date));
        $detail_saldo=array();
        $data_saldo=$this->cekAllJurnal($date, $this->site_id);
        foreach ($data_saldo as $key => $value) {
            $saldo=DB::table('tbl_saldo_akun')
                    ->where('id_akun', $value->id_akun)
                    ->where('tanggal', 'like', $date.'%');
            if ($user->site_id != null) {
                $saldo->where('location_id', $user->site_id);
            }
            $results=$saldo->first();
            $detail_saldo[$key]['data']=$value;
            $detail_saldo[$key]['detail']=$results;
        }
        $data=array(
            'date'      => $date,
            'bulan'     => json_encode(explode('-', $date)),
            'detail_saldo'      => $detail_saldo,
        );
        
        return  view('pages.akuntansi.neraca_list')->with(compact('data', 'bulan'));
    }
    public function labaRugi(Request $request){
       
        $user_id = request()->session()->get('user.id');
        $user = DB::table('users')->where('id', $this->user_id)->first();

        $date=0;
        if ($request->input('bulan')) {
            $date=$request->input('tahun').'-'.$request->input('bulan');
        }else{
            $date=date('Y-m');
        }
        $data['bulan']=json_encode(explode('-', $date));
        $data['pendapatan']=$this->getLabaRugi(6, $date, $user->site_id);
        $data['beban']=$this->getLabaRugi(7, $date, $user->site_id);
        return  view('pages.akuntansi.profit_loss_list')->with(compact('data'));
    }
    private function cekAllJurnal($date, $location_id){
        $results = DB::select( DB::raw("SELECT COALESCE((SELECT SUM(trd.jumlah) as jumlah FROM tbl_trx_akuntansi_detail 
            trd JOIN tbl_trx_akuntansi tra ON trd.id_trx_akun=tra.id_trx_akun JOIN tbl_akun_detail tad ON 
            tad.id_akun=trd.id_akun WHERE CAST(tra.tanggal AS TEXT) LIKE '".$date."%' ".($location_id != null ? 'AND tra.location_id='.$location_id : '')." AND tad.id_akun=tbl_akun_detail.id_akun 
            AND trd.tipe='KREDIT'), 0) + COALESCE((SELECT SUM(trd.jumlah) as jumlah FROM tbl_trx_akuntansi_detail trd 
            JOIN tbl_trx_akuntansi tra ON trd.id_trx_akun=tra.id_trx_akun JOIN tbl_akun_detail tad ON 
            tad.id_akun=trd.id_akun WHERE CAST(tra.tanggal AS TEXT) LIKE '".$date."%' ".($location_id != null ? 'AND tra.location_id='.$location_id : '')." AND tad.turunan1=tbl_akun_detail.id_akun AND 
            trd.tipe='KREDIT'), 0) AS jumlah_kredit, COALESCE((SELECT SUM(trd.jumlah) as jumlah 
            FROM tbl_trx_akuntansi_detail trd JOIN tbl_trx_akuntansi tra ON trd.id_trx_akun=tra.id_trx_akun JOIN 
            tbl_akun_detail tad ON tad.id_akun=trd.id_akun WHERE CAST(tra.tanggal AS TEXT) LIKE '".$date."%' ".($location_id != null ? 'AND tra.location_id='.$location_id : '')." AND 
            tad.id_akun=tbl_akun_detail.id_akun AND trd.tipe='DEBIT'), 0) + COALESCE((SELECT SUM(trd.jumlah) as jumlah 
            FROM tbl_trx_akuntansi_detail trd JOIN tbl_trx_akuntansi tra ON trd.id_trx_akun=tra.id_trx_akun JOIN 
            tbl_akun_detail tad ON tad.id_akun=trd.id_akun WHERE CAST(tra.tanggal AS TEXT) LIKE '".$date."%' ".($location_id != null ? 'AND tra.location_id='.$location_id : '')." AND 
            tad.turunan1=tbl_akun_detail.id_akun AND trd.tipe='DEBIT'), 0) AS jumlah_debit, MAX(tbl_akun_detail.id_akun) AS 
            id_akun, MAX(tbl_akun.no_akun) AS no_akun, MAX(tbl_akun.nama_akun) AS nama_akun, MAX(tbl_akun_detail.turunan1) AS 
            turunan1, MAX(tbl_akun_detail.id_parent) AS id_parent FROM tbl_akun_detail JOIN tbl_akun ON 
            tbl_akun_detail.id_akun=tbl_akun.id_akun WHERE turunan1=0 GROUP BY tbl_akun_detail.id_akun ORDER BY no_akun"));
        return $results;
    }

    private function getLabaRugi($id, $date, $location_id){
        // $results = DB::select( DB::raw('SELECT COALESCE((SELECT SUM(trd.jumlah) as jumlah FROM tbl_trx_akuntansi_detail 
        //     trd JOIN tbl_trx_akuntansi tra ON trd.id_trx_akun=tra.id_trx_akun JOIN tbl_akun_detail tad ON 
        //     tad.id_akun=trd.id_akun WHERE CAST(tra.tanggal AS TEXT) LIKE "'.$date.'%" '.($location_id != null ? "AND tra.location_id=".$location_id : "").' AND tad.id_akun=tbl_akun_detail.id_akun 
        //     AND trd.tipe="KREDIT"), 0) + COALESCE((SELECT SUM(trd.jumlah) as jumlah FROM tbl_trx_akuntansi_detail trd 
        //     JOIN tbl_trx_akuntansi tra ON trd.id_trx_akun=tra.id_trx_akun JOIN tbl_akun_detail tad ON 
        //     tad.id_akun=trd.id_akun WHERE CAST(tra.tanggal AS TEXT) LIKE "'.$date.'%" '.($location_id != null ? "AND tra.location_id=".$location_id : "").' AND tad.turunan1=tbl_akun_detail.id_akun AND 
        //     trd.tipe="KREDIT"), 0) AS jumlah_kredit, COALESCE((SELECT SUM(trd.jumlah) as jumlah 
        //     FROM tbl_trx_akuntansi_detail trd JOIN tbl_trx_akuntansi tra ON trd.id_trx_akun=tra.id_trx_akun JOIN 
        //     tbl_akun_detail tad ON tad.id_akun=trd.id_akun WHERE CAST(tra.tanggal AS TEXT) LIKE "'.$date.'%" '.($location_id != null ? "AND tra.location_id=".$location_id : "").' AND 
        //     tad.id_akun=tbl_akun_detail.id_akun AND trd.tipe="DEBIT"), 0) + COALESCE((SELECT SUM(trd.jumlah) as jumlah 
        //     FROM tbl_trx_akuntansi_detail trd JOIN tbl_trx_akuntansi tra ON trd.id_trx_akun=tra.id_trx_akun JOIN 
        //     tbl_akun_detail tad ON tad.id_akun=trd.id_akun WHERE CAST(tra.tanggal AS TEXT) LIKE "'.$date.'%" '.($location_id != null ? "AND tra.location_id=".$location_id : "").' AND 
        //     tad.turunan1=tbl_akun_detail.id_akun AND trd.tipe="DEBIT"), 0) AS jumlah_debit, MAX(tbl_akun_detail.id_akun) AS 
        //     id_akun, MAX(tbl_akun.no_akun) AS no_akun, MAX(tbl_akun.nama_akun) AS nama_akun, MAX(tbl_akun_detail.turunan1) AS 
        //     turunan1, MAX(tbl_akun_detail.id_parent) AS id_parent FROM tbl_akun_detail JOIN tbl_akun ON 
        //     tbl_akun_detail.id_akun=tbl_akun.id_akun WHERE turunan1=0 AND id_parent='.$id.' GROUP BY tbl_akun_detail.id_akun ORDER BY tbl_akun.no_akun'));
        $results = DB::select( DB::raw("SELECT COALESCE((SELECT SUM(trd.jumlah) as jumlah FROM tbl_trx_akuntansi_detail 
            trd JOIN tbl_trx_akuntansi tra ON trd.id_trx_akun=tra.id_trx_akun JOIN tbl_akun_detail tad ON 
            tad.id_akun=trd.id_akun WHERE CAST(tra.tanggal AS TEXT) LIKE '".$date."%' ".($location_id != null ? 'AND tra.location_id='.$location_id : '')." AND tad.id_akun=tbl_akun_detail.id_akun 
            AND trd.tipe='KREDIT'), 0) + COALESCE((SELECT SUM(trd.jumlah) as jumlah FROM tbl_trx_akuntansi_detail trd 
            JOIN tbl_trx_akuntansi tra ON trd.id_trx_akun=tra.id_trx_akun JOIN tbl_akun_detail tad ON 
            tad.id_akun=trd.id_akun WHERE CAST(tra.tanggal AS TEXT) LIKE '".$date."%' ".($location_id != null ? 'AND tra.location_id='.$location_id : '')."  AND tad.turunan1=tbl_akun_detail.id_akun AND 
            trd.tipe='KREDIT'), 0) AS jumlah_kredit, COALESCE((SELECT SUM(trd.jumlah) as jumlah 
            FROM tbl_trx_akuntansi_detail trd JOIN tbl_trx_akuntansi tra ON trd.id_trx_akun=tra.id_trx_akun JOIN 
            tbl_akun_detail tad ON tad.id_akun=trd.id_akun WHERE CAST(tra.tanggal AS TEXT) LIKE '".$date."%' ".($location_id != null ? 'AND tra.location_id='.$location_id : '')."  AND 
            tad.id_akun=tbl_akun_detail.id_akun AND trd.tipe='DEBIT'), 0) + COALESCE((SELECT SUM(trd.jumlah) as jumlah 
            FROM tbl_trx_akuntansi_detail trd JOIN tbl_trx_akuntansi tra ON trd.id_trx_akun=tra.id_trx_akun JOIN 
            tbl_akun_detail tad ON tad.id_akun=trd.id_akun WHERE CAST(tra.tanggal AS TEXT) LIKE '".$date."%' ".($location_id != null ? 'AND tra.location_id='.$location_id : '')."  AND 
            tad.turunan1=tbl_akun_detail.id_akun AND trd.tipe='DEBIT'), 0) AS jumlah_debit, MAX(tbl_akun_detail.id_akun) AS 
            id_akun, MAX(tbl_akun.no_akun) AS no_akun, MAX(tbl_akun.nama_akun) AS nama_akun, MAX(tbl_akun_detail.turunan1) AS 
            turunan1, MAX(tbl_akun_detail.id_parent) AS id_parent FROM tbl_akun_detail JOIN tbl_akun ON 
            tbl_akun_detail.id_akun=tbl_akun.id_akun WHERE turunan1=0 AND id_parent='".$id."' GROUP BY tbl_akun_detail.id_akun ORDER BY no_akun"));
        return $results;
    }
    /**
     * Show the form for creating a new resource.
     * @return Response
     */

    public function createJournal()
    {

        $user = DB::table('users')->where('id', $this->user_id)->first();
        $location_id = $user->site_id;

        $business_locations = DB::table('sites')->get();
        $data['akun_option']=array();
        $data['akun_option'][''] = 'Pilih Akun / No Akun';
        $akun_option_js=array();
        $count=$c=0;
        $dataLevel0=DB::table('tbl_akun')->where('level', 0)->get();
        foreach ($dataLevel0 as $key => $value) {
            $id_akun=$no_akun=$nama_akun=0;
            $dataLevel1=DB::table('tbl_akun')->where('level', 1)->where('id_main_akun', $value->id_akun)->get();
            foreach ($dataLevel1 as $k => $v) {
                $id_akun=$v->id_akun;
                $no_akun=$v->no_akun;
                $nama_akun=$v->nama_akun;
                $dataLevel2=DB::table('tbl_akun')->where('level', 2)->where('id_main_akun', $v->id_akun)->get();
                foreach ($dataLevel2 as $k2 => $v2) {
                    $id_akun=$v2->id_akun;
                    $no_akun=$v2->no_akun;
                    $nama_akun=$v2->nama_akun;
                    $dataLevel3=DB::table('tbl_akun')->where('level', 3)->where('id_main_akun', $v2->id_akun)->get();
                    foreach ($dataLevel3 as $k3 => $v3) {
                        $c++;
                        $id_akun=$v3->id_akun;
                        $no_akun=$v3->no_akun;
                        $nama_akun=$v3->nama_akun;
                        $data['akun_option'][$id_akun]=$no_akun. ' | '.$nama_akun;
                        $akun_option_js[]=array(
                            'label'     => $id_akun,
                            'value'     => $no_akun. ' | '.$nama_akun
                        );
                    }
                    $data['akun_option'][$id_akun]=$no_akun. ' | '.$nama_akun;
                    $akun_option_js[]=array(
                        'label'     => $id_akun,
                        'value'     => $no_akun. ' | '.$nama_akun
                    );
                }
                $data['akun_option'][$id_akun]=$no_akun. ' | '.$nama_akun;
                $akun_option_js[]=array(
                    'label'     => $id_akun,
                    'value'     => $no_akun. ' | '.$nama_akun
                );
            }
        }
        $data['akun_option_js']=json_encode($akun_option_js);
        
        return  view('pages.akuntansi.create_journal')
                ->with(compact('data', 'business_locations', 'location_id', 'user'));
    }

    public function storeJurnal(Request $request)
    {
        
        $deskripsi=$request->input('deskripsi');
        $location_id=$request->input('location_id');
        $tgl=$request->input('tanggal');
        $id_akun=$request->input('akun');
        $jumlah=$request->input('jumlah_akun');
        $tipe_akun=$request->input('tipe_akun');
        $id_lawan=$request->input('lawan_akun');
        $jumlah_lawan=$request->input('jumlah_lawan_akun');
        $tipe_akun_lawan=$request->input('tipe_lawan_akun');
        $data_trx=array(
                        'deskripsi'     => $deskripsi,
                        'location_id'     => $location_id,
                        'tanggal'       => $tgl,
                    );
        $insert=DB::table('tbl_trx_akuntansi')->insert($data_trx);
        // $insert=1;
        if ($insert) {
            $id_last=DB::table('tbl_trx_akuntansi')->max('id_trx_akun');
            $data=array(
                        'id_trx_akun'   => $id_last,
                        'id_akun'       => $id_akun,
                        'jumlah'        => $jumlah,
                        'tipe'          => ($request->input('tipe_akun') == 0 ? "KREDIT" : "DEBIT"),
                        'keterangan'    => 'akun',
                    );
            DB::table('tbl_trx_akuntansi_detail')->insert($data);
            for ($i=0; $i < count($id_lawan); $i++) { 
                if ($id_lawan[$i] != null) {
                    $data=array(
                        'id_trx_akun'   => $id_last,
                        'id_akun'       => $id_lawan[$i],
                        'jumlah'        => $jumlah_lawan[$i],
                        'tipe'          => ($tipe_akun_lawan[$i] == 0 ? "KREDIT" : "DEBIT"),
                        'keterangan'    => 'lawan',
                    );
                    // $this->updateSaldo($id_lawan[$i], $jumlah_lawan[$i], $tipe_akun_lawan[$i]);
                    DB::table('tbl_trx_akuntansi_detail')->insert($data);
                }
            }
        }
        return redirect('akuntansi/jurnal');
    }
    public function closeBook(Request $request){
        
        $user_id = $this->user_id;
        $user = DB::table('users')->where('id', $user_id)->first();
        
        $location_id = $user->site_id;

        $date=0;
        if ($request->input('bulan')) {
            $date=$request->input('tahun').'-'.$request->input('bulan');
        }else{
            $date=date('Y-m');
        }

        $bulan=explode('-', $date);
        if ($request->input('submit')) {
            // $business_id=($request->session()->get('user.business_id'));
            // $business=DB::table('business')->where('id', $business_id)->first();
            
            $time = strtotime($date);
            $final = date('Y-m', strtotime("+1 month", $time));
            $cekJurnal=DB::table('tbl_saldo_akun')->where('tanggal', $final)->where('location_id', $location_id)->count();
            // if ($cekJurnal < 1) {
            $jurnal=$this->cekAllJurnal($date, null);
            $total_pendapatan=$total_beban=$total_kas=0;
            foreach ($jurnal as $key => $value) {
                $saldo=DB::table('tbl_saldo_akun')->where('id_akun', $value->id_akun)->where('location_id', $location_id)->where('tanggal', $date)->first();
                if ($value->id_parent == 3 || $value->id_parent == 7) {
                    $jumlah_saldo=(($saldo != null ? $saldo->jumlah_saldo : 0) + $value->jumlah_debit) - $value->jumlah_kredit;
                    if ($value->id_parent == 7) {
                        $total_beban+=($value->jumlah_debit - $value->jumlah_kredit);
                    }
                    if ($value->id_akun == 20) {
                        $total_kas=$jumlah_saldo;
                    }
                    $data_saldo=array(
                        'id_akun'   => $value->id_akun,
                        'jumlah_saldo'  => $jumlah_saldo,
                        'tanggal'   => $final,
                        'location_id'   => $location_id,
                        'is_updated'   => 0,
                    );
                    $ceksaldo=DB::table('tbl_saldo_akun')->where('tanggal', $final)->where('location_id', $location_id)->where('id_akun', $value->id_akun)->first();
                    if ($ceksaldo == null) {
                        DB::table('tbl_saldo_akun')->insert($data_saldo);
                    }else{
                        DB::table('tbl_saldo_akun')->where('id_saldo', $ceksaldo->id_saldo)->update($data_saldo);
                    }
                }else{
                    if ($value->id_akun != 56) {
                        $jumlah_saldo=(($saldo != null ? $saldo->jumlah_saldo : 0) + $value->jumlah_kredit) - $value->jumlah_debit;
                        if ($value->id_parent == 6) {
                            $total_pendapatan+=($value->jumlah_kredit - $value->jumlah_debit);
                        }
                        $data_saldo=array(
                            'id_akun'   => $value->id_akun,
                            'jumlah_saldo'  => $jumlah_saldo,
                            'tanggal'   => $final,
                            'location_id'   => $location_id,
                            'is_updated'   => 0,
                        );
                        $ceksaldo=DB::table('tbl_saldo_akun')->where('tanggal', $final)->where('location_id', $location_id)->where('id_akun', $value->id_akun)->first();
                        if ($ceksaldo == null) {
                            DB::table('tbl_saldo_akun')->insert($data_saldo);
                        }else{
                            DB::table('tbl_saldo_akun')->where('id_saldo', $ceksaldo->id_saldo)->update($data_saldo);
                        }
                    }
                }
            }
            $output = ['success' => 1,
                            'msg' => 'success'
                        ];
            return redirect('akuntansi/close-book')->with('status', $output);
        }
        return  view('pages.akuntansi.close_book')
                ->with(compact('bulan'));
    }
    public function rekapPc(Request $request){
        
        $user_id = request()->session()->get('user.id');
        $user = User::where('id', $user_id)->first();
        $location_id = $user->location_id;

        $date=0;
        if ($request->input('bulan')) {
            $date=$request->input('tahun').'-'.$request->input('bulan');
            $jumlah_hari=cal_days_in_month(CAL_GREGORIAN, $request->input('bulan'), $request->input('tahun'));
        }else{
            $date=date('Y-m');
            $jumlah_hari=cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));
        }
        $detail=array();
        $bulan=json_encode(explode('-', $date));
        for ($i=1; $i <= $jumlah_hari; $i++) { 
            $tanggal=$date.'-'.(strlen($i) < 2 ? '0'.$i : $i);
            $detail[$i]['date']=$tanggal;
            $list_pc=$this->listPettyCashByDate($tanggal, $location_id);
            foreach ($list_pc as $key => $value) {
                $detail[$i]['data'][$key]=$this->getTrxPetty($value->id_trx_akun, $location_id);
            }
        }
        return  view('pages.akuntansi.rekap_pc')
                ->with(compact('detail', 'bulan'));
    }
    public function rekapTransaksi(Request $request){
        
        $user_id = request()->session()->get('user.id');
        $user = User::where('id', $user_id)->first();
        $location_id = $user->location_id;

        $date=0;
        if ($request->input('bulan')) {
            $date=$request->input('tahun').'-'.$request->input('bulan');
            $jumlah_hari=cal_days_in_month(CAL_GREGORIAN, $request->input('bulan'), $request->input('tahun'));
        }else{
            $date=date('Y-m');
            $jumlah_hari=cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));
        }
        $detail=array();
        $bulan=json_encode(explode('-', $date));
        for ($i=1; $i <= $jumlah_hari; $i++) { 
            $tanggal=$date.'-'.(strlen($i) < 2 ? '0'.$i : $i);
            $detail[$i]['date']=$tanggal;
            $list_pc=$this->listTrx($tanggal, $location_id);
            foreach ($list_pc as $key => $value) {
                $detail[$i]['data'][$key]=$this->getTrx($value->id_trx_akun, $location_id);
            }
        }
        
        return  view('pages.akuntansi.rekap_trx')
                ->with(compact('detail', 'bulan'));
    }
    private function getTrxPetty($id, $location_id){
        $query = DB::table('tbl_trx_akuntansi_detail')
                            ->join('tbl_trx_akuntansi', 'tbl_trx_akuntansi.id_trx_akun', '=', 'tbl_trx_akuntansi_detail.id_trx_akun')
                            ->join('tbl_akun', 'tbl_akun.id_akun', '=', 'tbl_trx_akuntansi_detail.id_akun')
                            ->select('tbl_trx_akuntansi_detail.*','tbl_akun.*', 'tbl_trx_akuntansi.deskripsi')
                            ->where('tbl_trx_akuntansi_detail.id_trx_akun', $id)
                            ->where('tipe', 'DEBIT');

        if ($location_id != null) {
            $query->where('tbl_trx_akuntansi.location_id', $location_id);
        }
        $results=$query->get();
        return $results;
    }
    private function getTrx($id, $location_id){
        $query = DB::table('tbl_trx_akuntansi_detail')
                            ->join('tbl_trx_akuntansi', 'tbl_trx_akuntansi.id_trx_akun', '=', 'tbl_trx_akuntansi_detail.id_trx_akun')
                            ->join('tbl_akun', 'tbl_akun.id_akun', '=', 'tbl_trx_akuntansi_detail.id_akun')
                            ->where('tbl_trx_akuntansi_detail.id_trx_akun', $id)
                            // ->whereIn('tbl_trx_akuntansi_detail.id_akun', [91, 39])
                            ->where('tipe', 'KREDIT')
                            ->select('tbl_trx_akuntansi_detail.*','tbl_akun.*', 'tbl_trx_akuntansi.deskripsi');
        if ($location_id != null) {
            $query->where('tbl_trx_akuntansi.location_id', $location_id);
        }
        $results=$query->get();
        return $results;
    }
    private function listPettyCashByDate($date, $location_id){
        $query=DB::table('tbl_trx_akuntansi')
                            ->join('tbl_trx_akuntansi_detail', 'tbl_trx_akuntansi.id_trx_akun', '=', 'tbl_trx_akuntansi_detail.id_trx_akun')
                            ->select('tbl_trx_akuntansi.id_trx_akun')
                            ->where('tanggal', $date)
                            ->where('id_akun', 35);
                            
        if ($location_id != null) {
            $query->where('tbl_trx_akuntansi.location_id', $location_id);
        }
        $results=$query->get();
        return $results;
    }
    private function listTrx($date, $location_id){
        $query= DB::table('tbl_trx_akuntansi')
                            ->join('tbl_trx_akuntansi_detail', 'tbl_trx_akuntansi.id_trx_akun', '=', 'tbl_trx_akuntansi_detail.id_trx_akun')
                            ->where('tanggal', $date)
                            ->whereIn('id_akun', [91, 39])
                            // ->orWhere('id_akun','=', 39)
                            ->select('tbl_trx_akuntansi.id_trx_akun')
                            ->groupBy('tbl_trx_akuntansi.id_trx_akun');
        if ($location_id != null) {
            $query->where('tbl_trx_akuntansi.location_id', $location_id);
        }
        $results=$query->get();
        return $results;
    }
    // private function listTrx($date){
    //     $results = DB::select('SELECT * FROM tbl_trx_akuntansi JOIN tbl_trx_akuntansi_detail ON 
    //         tbl_trx_akuntansi_detail.id_trx_akun=tbl_trx_akuntansi.id_trx_akun WHERE tbl_trx_akuntansi_detail.id_akun=91 
    //         OR tbl_trx_akuntansi_detail.id_akun=39 AND tanggal="'.$date.'"');
    //     return $results;
    // }
    // private function listPettyCashByDate($date){
    //     $results = DB::select( DB::raw('SELECT COALESCE((SELECT SUM(jumlah) FROM tbl_trx_akuntansi_detail trd1 JOIN 
    //         tbl_trx_akuntansi tra1 ON trd1.id_trx_akun=tra1.id_trx_akun WHERE tra1.tanggal="'.$date.'" AND trd1.tipe="KREDIT" 
    //         AND trd1.id_akun=trd.id_akun),0) AS jumlah_kredit, COALESCE((SELECT SUM(jumlah) FROM tbl_trx_akuntansi_detail trd1 
    //         JOIN tbl_trx_akuntansi tra1 ON trd1.id_trx_akun=tra1.id_trx_akun WHERE tra1.tanggal="'.$date.'" AND trd1.tipe="DEBIT" 
    //         AND trd1.id_akun=trd.id_akun),0) AS jumlah_debit, ta.no_akun, ta.id_akun, ta.nama_akun, tra.deskripsi, CAST(tra.tanggal AS TEXT), 
    //         trd.tipe FROM tbl_trx_akuntansi tra JOIN tbl_trx_akuntansi_detail trd ON tra.id_trx_akun=trd.id_trx_akun JOIN 
    //         tbl_akun ta ON ta.id_akun=trd.id_akun WHERE CAST(tra.tanggal AS TEXT)="'.$date.'" AND trd.tipe="DEBIT" AND tra.id_trx_akun IN 
    //         (SELECT id_trx_akun FROM tbl_trx_akuntansi_detail WHERE id_akun=35) GROUP BY id_akun ORDER BY no_akun'));
    //     return $results;
    // }
}
