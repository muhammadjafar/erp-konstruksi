<?php

namespace App\Http\Controllers\INV;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use App\Http\Controllers\RAB\RabController;
use Carbon\Carbon;
use DB;
class OrderController extends Controller
{
    private $base_api_url;
    private $site_id = null;
    private $username = null;
    public function __construct()
    {
        //Authenticate page menu
        $this->middleware(function ($request, $next) {
            Controller::isLogin(auth()->user()['role_id']);
            $this->site_id = auth()->user()['site_id']; 
            $this->username = auth()->user()['email'];
            return $next($request);
        });

        $this->base_api_url = env('API_URL');
    }

    public function index()
    {
        $is_error = false;
        $error_message = '';  

        $data = array(
            'error' => array(
                'is_error' => $is_error,
                'error_message' => $error_message
            )
        );
        
        return view('pages.inv.order.order_list', $data);
    }
    public function create()
    {
        $customer = null;
        try
        {
            $client = new Client(['base_uri' => $this->base_api_url . '/crm/customerdatamain']);  
            $response = $client->request('GET', ''); 
            $body = $response->getBody();
            $content = $body->getContents();
            $response_array = json_decode($content,TRUE);
            $customer = $response_array['data'];
        } catch(RequestException $exception) {      
        }
        
        $data = array(
            'customer' => $customer
        );
        return view('pages.inv.order.order_create', $data);
    }
    public function edit($id)
    {
        $customer = null;
        try
        {
            $client = new Client(['base_uri' => $this->base_api_url . '/crm/customerdatamain']);  
            $response = $client->request('GET', ''); 
            $body = $response->getBody();
            $content = $body->getContents();
            $response_array = json_decode($content,TRUE);
            $customer = $response_array['data'];
        } catch(RequestException $exception) {      
        }

        try
        {
            $client = new Client(['base_uri' => $this->base_api_url . 'inv/base/Order/' . $id]);  
            $response = $client->request('GET', ''); 
            $body = $response->getBody();
            $content = $body->getContents();
            $response_array = json_decode($content,TRUE);
            
            $order = $response_array['data'];
        } catch(RequestException $exception) {    
        }

        try
        {
            $client = new Client(['base_uri' => $this->base_api_url . 'order/order_d/' . $id]);  
            $response = $client->request('GET', ''); 
            $body = $response->getBody();
            $content = $body->getContents();
            $response_array = json_decode($content,TRUE);
            
            $order_d = $response_array['data'];
        } catch(RequestException $exception) {    
        }
        
        
        $data = array(
            'customer' => $customer,
            'order' => $order,
            'order_d' => $order_d
        );
        
        return view('pages.inv.order.order_edit', $data);
    }
    public function suggestProduct(Request $request){
        if($request->has('q')){
            $key=$request->q;
            $data=DB::table('products')->where('name', 'like', '%'.$key.'%')->get();
            return $data;
        }
    }
    function fetch(Request $request)
    {
        $search = $request->search;
        
        if($search == ''){
            $data=DB::table('products')->get();
        }else{
            $data=DB::table('products')->where('name', 'ilike', '%'.$search.'%')->whereNull('deleted_at')->limit(15)->get();
        }
  
        $response = array();
        foreach($data as $data){
           $response[] = array("value"=>$data->id,"label"=>$data->name);
        }
        return response()->json($response);
        // echo json_encode($response);
    }
    public function getProduct($id){
        $data=DB::table('products')->where('id', $id)->first();
        return response()->json($data);
    }
    public function save(Request $request){
        $period_year = date('Y');
        $period_month = date('m');
        $editedProd=$request->input('editProd');
        $id_product=$request->input('id_product');
        $nameProd=$request->input('name');
        $deskripsi=$request->input('deskripsi');
        $file=$request->file('file');
        
        if($editedProd != null){
            for ($i=0; $i < count($editedProd); $i++) { 
                $index=array_search($editedProd[$i], $id_product);
                $detail=DB::table('products')->where('id', $id_product[$index])->first();
                $image=$detail->image;
                if($file[$index]){
                    $tujuan_upload = 'upload/product';
                        // upload file
                    $name=time().'.'.$file[$index]->getClientOriginalExtension();
                    $file[$index]->move($tujuan_upload, $name);
                    
                    $image=$name;
                }
                $data=array(
                    'name'          => $nameProd[$index],
                    'description'   => $deskripsi[$index],
                    'image'         => $image,
                    'm_unit_id'      => $detail->m_unit_id,
                    'price'      => $detail->price,
                );
                DB::table('products')->insert($data);
                $last_id=DB::table('products')->max('id');
                $id_product[$index]=$last_id;
            }
        }
        
        $rabcon = new RabController();
        $ord_no = $rabcon->generateTransactionNo('ORD', $period_year, $period_month, $this->site_id );
        try
        {
            $client = new Client(['base_uri' => $this->base_api_url . 'inv/base/Order']);
            $reqBody = [
                'json' => [
                    'customer_id' => $request->input('customer_id'),
                    'order_name' => $request->input('order_name'),
                    'order_date' => date('Y-m-d'),
                    'is_done' => 0,
                    'site_id' => $this->site_id,
                    'order_no'  => $ord_no
                ]
            ]; 
            $response = $client->request('POST', '', $reqBody); 
        } catch(RequestException $exception) {
        }
        $order=DB::table('orders')->where('order_no', $ord_no)->first();

        if($response){
            $total_produk=$request->input('total_produk');
            for ($i=0; $i < count($id_product); $i++) { 
                $cek_project=DB::table('projects')->where('product_id', $id_product[$i])->first();
                if($cek_project == null){
                    // create project from product
                    $product=DB::table('products')->where('id', $id_product[$i])->first();
                    try
                    {
                        $client = new Client(['base_uri' => $this->base_api_url . 'inv/base/Project']);
                        $reqBody = [
                            'json' => [
                                'site_id'       => $this->site_id,
                                'name'          => 'Project '.$product->name,
                                'base_price'    => 0,
                                'customer_id'   => $request->input('customer_id'),
                                'sale_status'   => 'Available',
                                'product_id'      => $id_product[$i],
                                'order_id'      => $order->id,
                            ]
                        ]; 
                        $response = $client->request('POST', '', $reqBody); 
                    } catch(RequestException $exception) {
                    }
                }
                //create order detail
                try
                {
                    $client = new Client(['base_uri' => $this->base_api_url . 'inv/base/OrderD']);
                    $reqBody = [
                        'json' => [
                            'order_id' => $order->id,
                            'product_id' => $id_product[$i],
                            'in_rab' => 0,
                            'total'     => $total_produk[$i]
                        ]
                    ]; 
                    $response_pd = $client->request('POST', '', $reqBody); 
                } catch(RequestException $exception) {
                }
                $order_d=DB::table('order_ds')->max('id');
                for ($j=1; $j <= $total_produk[$i]; $j++) { 
                    $ord_no_detail=$ord_no.'/'.$id_product[$i].'/'.$j;
                    try
                    {
                        $client = new Client(['base_uri' => $this->base_api_url . 'inv/base/ProductSub']);
                        $reqBody = [
                            'json' => [
                                'product_id' => $id_product[$i],
                                'order_d_id' => $order_d,
                                'no' => $ord_no_detail,
                            ]
                        ]; 
                        $response_pd = $client->request('POST', '', $reqBody); 
                    } catch(RequestException $exception) {
                    }

                    // $product_sub=DB::table('product_subs')->where('no', $ord_no_detail)->first();
                }
            }
            $notification = array(
                'message' => 'Success add Order',
                'alert-type' => 'success'
            );
        }else{
            $notification = array(
                'message' => 'Failed add Order',
                'alert-type' => 'error'
            );
        }

        return redirect('/order')->with('notification');
    }

    public function update(Request $request){
        $id=$request->input('order_id');
        $deleted_order_d=$request->input('deleted_order_d');
        $order_d_id=$request->input('order_d_id');
        $period_year = date('Y');
        $period_month = date('m');
        $editedProd=$request->input('editProd');
        $id_product=$request->input('id_product');
        $nameProd=$request->input('name');
        $deskripsi=$request->input('deskripsi');
        $file=$request->file('file');

        //set produk baru berdasarkan row
        if($editedProd != null){
            for ($i=0; $i < count($editedProd); $i++) { 
                $index=array_search($editedProd[$i], $id_product);
                $detail=DB::table('products')->where('id', $id_product[$index])->first();
                $image=$detail->image;
                if($file[$index]){
                    $tujuan_upload = 'upload/product';
                        // upload file
                    $name=time().'.'.$file[$index]->getClientOriginalExtension();
                    $file[$index]->move($tujuan_upload, $name);
                    
                    $image=$name;
                }
                $data=array(
                    'name'          => $nameProd[$index],
                    'description'   => $deskripsi[$index],
                    'image'         => $image,
                    'm_unit_id'      => $detail->m_unit_id,
                    'price'      => $detail->price,
                );
                DB::table('products')->insert($data);
                $last_id=DB::table('products')->max('id');
                $id_product[$index]=$last_id;
            }
        }
        //get data order
        $order=DB::table('orders')->where('id', $id)->first();

        try
        {
            $client = new Client(['base_uri' => $this->base_api_url . 'inv/base/Order/' . $id]);  
            $reqBody = [
                'json' => [
                    'customer_id' => $request->input('customer_id'),
                    'order_name' => $request->input('order_name'),
                    'order_date' => date('Y-m-d'),
                    'is_done' => $order->is_done,
                    'site_id' => $this->site_id,
                    'order_no'  => $order->order_no
                ]
            ]; 
            $response = $client->request('PUT', '', $reqBody); 
        } catch(RequestException $exception) {    
        }
        
        if($response){
            $total_produk=$request->input('total_produk');
            for ($i=0; $i < count($id_product); $i++) { 
                if($order_d_id[$i] != 0){//jika order id tidak kosong (menandakan order detail sudah ada sebelumnya), maka edit order detail
                    //jika order di daftar hapus, maka command dibawah ini
                    if($deleted_order_d != null){
                        if (in_array($order_d_id[$i], $deleted_order_d)){
                            DB::table('order_ds')->where('id', $order_d_id[$i])->delete();//hapus order detail yang ada dalam list delete
                        }
                    }else{//jika tidak ditemukan di order hapus
                        $get_detail=DB::table('order_ds')->where('id', $order_d_id[$i])->first();
                        if($get_detail->total == $total_produk[$i]){
                            //do nothing
                        }else if($get_detail->total > $total_produk[$i]){
                            $selisih=$get_detail->total - $total_produk[$i];
                            DB::table('order_ds')->where('id', $order_d_id[$i])->update(['total' => $total_produk[$i]]);//update order detail
                            DB::delete('delete from product_subs where id in 
                            (SELECT id from product_subs where order_d_id='.$order_d_id[$i].' order by id desc limit '.$selisih.')');//menghapus pengurangan total order produk
                        }else{
                            DB::table('order_ds')->where('id', $order_d_id[$i])->update(['total' => $total_produk[$i]]);//update order detail
                            for ($j=$get_detail->total + 1; $j <= $total_produk[$i]; $j++) { //perulangan untuk menambah penambahan total order produk
                                $ord_no_detail=$order->order_no.'/'.$id_product[$i].'/'.$j;
                                try
                                {
                                    $client = new Client(['base_uri' => $this->base_api_url . 'inv/base/ProductSub']);
                                    $reqBody = [
                                        'json' => [
                                            'product_id' => $id_product[$i],
                                            'order_d_id' => $order_d_id[$i],
                                            'no' => $ord_no_detail,
                                        ]
                                    ]; 
                                    $response_pd = $client->request('POST', '', $reqBody); 
                                } catch(RequestException $exception) {
                                }
                            }
                        }
                    }
                }else{
                    try
                    {
                        $client = new Client(['base_uri' => $this->base_api_url . 'inv/base/OrderD']);
                        $reqBody = [
                            'json' => [
                                'order_id' => $order->id,
                                'product_id' => $id_product[$i],
                                'in_rab' => 0,
                                'total'     => $total_produk[$i]
                            ]
                        ]; 
                        $response_pd = $client->request('POST', '', $reqBody); 
                    } catch(RequestException $exception) {
                    }
                    $order_d=DB::table('order_ds')->max('id');
                    for ($j=1; $j <= $total_produk[$i]; $j++) { 
                        $ord_no_detail=$order->order_no.'/'.$id_product[$i].'/'.$j;
                        try
                        {
                            $client = new Client(['base_uri' => $this->base_api_url . 'inv/base/ProductSub']);
                            $reqBody = [
                                'json' => [
                                    'product_id' => $id_product[$i],
                                    'order_d_id' => $order_d,
                                    'no' => $ord_no_detail,
                                ]
                            ]; 
                            $response_pd = $client->request('POST', '', $reqBody); 
                        } catch(RequestException $exception) {
                        }
                    }
                }
                
            }
            $notification = array(
                'message' => 'Success edit Order',
                'alert-type' => 'success'
            );
        }else{
            $notification = array(
                'message' => 'Failed edit Order',
                'alert-type' => 'error'
            );
        }
        
        return redirect('/order')->with('notification');
    }

    public function GetOrderJson() {
        $response = null;
        try
        {
            $client = new Client(['base_uri' => $this->base_api_url . 'order/list']); 
            $response = $client->request('GET', ''); 
            $body = $response->getBody();
            $content = $body->getContents();
            $response_array = json_decode($content,TRUE);

            $response = $content;  
            $data=DataTables::of($response_array['data'])
                                    ->addColumn('action', function ($row) {
                                        return '<button type="button" id="modal_detail" class="btn btn-success btn-sm" data-toggle="modal" data-order_no="'.$row['order_no'].'" data-id="'.$row['id'].'" data-target=".bs-example-modal-lg" onclick="getDetail(this)"><i class="mdi mdi-eye"></i></button>'.'
                                        
                                        '.'<a href="/order/delete/'.$row['id'].'" class="btn btn-danger btn-sm" onclick="javasciprt: return confirm(\'Are You Sure ?\')"><i class="mdi mdi-delete"></i></a>';
                                        // '.'<a href="/order/edit/'.$row['id'].'" class="btn btn-info btn-sm"><i class="mdi mdi-pencil"></i></a>'.'
                                    })
                                    ->make(true);          
        } catch(RequestException $exception) {
            
        }    

        return $data;
    }
    public function GetOrderDetailJson($id) {
        $response = null;
        try
        {
            $client = new Client(['base_uri' => $this->base_api_url . 'order/order_d/'.$id]); 
            $response = $client->request('GET', ''); 
            $body = $response->getBody();
            $content = $body->getContents();
            $response_array = json_decode($content,TRUE);

            $response = $content;  
            $data=DataTables::of($response_array['data'])
                                    ->make(true);          
        } catch(RequestException $exception) {
            
        }    

        return $data;
    }
    public function deleteOrder($id) {
        try
        {
            $client = new Client(['base_uri' => $this->base_api_url . 'inv/base/Order/' . $id]);  
            $response = $client->request('DELETE', ''); 
        } catch(RequestException $exception) {    
        }

        $notification = array(
            'message' => 'Success delete order',
            'alert-type' => 'success'
        );

        return redirect('order')->with($notification);
    }
}
