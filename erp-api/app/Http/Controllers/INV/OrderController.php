<?php
namespace App\Http\Controllers\INV;

use App\Models\Order;
use App\Models\OrderD;
use App\Models\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class OrderController extends Controller
{
    public function getOrderList()
    {
        $data = Order::all();
        foreach($data as $value){
            $customer=Customer::find($value->customer_id);
            $value->customer_name=$customer->name;
            $value->customer_coorporate=$customer->coorporate_name;
        }
        return response()->json(['data'=>$data]);
    }
    public function getOrderListJson($orderId)
    {
        $data = OrderD::where(['order_id'=> $orderId])
                ->join('products', 'products.id', '=', 'order_ds.product_id')
                ->select('products.name', 'products.image', 'products.description', 'order_ds.*')
                ->orderBy('order_ds.created_at','asc')
                ->get();
        return response()->json(['data'=>$data]);
    }
    public function getOrderListJsonNonRab($orderId)
    {
        $data = OrderD::where(['order_id'=> $orderId, 'in_rab' => 0])
                ->join('products', 'products.id', '=', 'order_ds.product_id')
                ->select('products.name', 'products.image', 'products.description', 'order_ds.*')
                ->orderBy('order_ds.created_at','asc')
                ->get();
        return response()->json(['data'=>$data]);
    }
    public function getOrderDetailById($orderId)
    {
        $data = OrderD::where(['order_id'=> $orderId])
                ->get();
        return response()->json(['data'=>$data]);
    }
}
